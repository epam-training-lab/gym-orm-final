package com.epam.gym.repository;

import com.epam.gym.entity.Trainee;
import com.epam.gym.entity.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
class UserRepositoryTest {

    @Autowired
    private UserRepository userRepository;
    private User testUser;

    @BeforeEach
    void setUp() {
        testUser = new Trainee(
                1L,
                "John",
                "Doe",
                "john.doe",
                "password",
                true,
                null,
                null
        );

        this.userRepository.save(testUser);
    }

    @Test
    void checkValidUserCredentialsTest() {
        assertTrue(userRepository.existsByUsernameAndPassword(
                testUser.getUsername(),
                testUser.getPassword()));
    }

    @Test
    void checkNonValidUsernameTest() {
        assertFalse(userRepository.existsByUsernameAndPassword("invalid", testUser.getPassword()));
    }

    @Test
    void checkNonValidPasswordTest() {
        assertFalse(userRepository.existsByUsernameAndPassword(testUser.getUsername(), "invalid"));
    }

    @Test
    void countUsersWithUsernameWithSamePrefix() {
        userRepository.save(new Trainee(
                1L,
                "John",
                "Doe",
                "john.doe2",
                "password",
                true,
                null,
                null));

        assertEquals(2, userRepository.countByUsernameStartingWith("john.doe"));
    }

}