package com.epam.gym.service;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

import com.epam.gym.dto.*;
import com.epam.gym.entity.*;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import com.epam.gym.repository.TraineeRepository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.function.BiPredicate;

@SpringBootTest
class TraineeServiceTest {

    @Autowired
    private TraineeService traineeService;

    @MockBean
    private TraineeRepository traineeRepository;

    @MockBean
    private UserService userService;

    @MockBean
    private TrainingService trainingService;

    private static BiPredicate<Trainee, Trainee> traineeEquals;
    private static Trainer trainer1;
    private static Trainer trainer2;
    private static Training training1;
    private static Training training2;
    private static Training training3;
    private static Trainee testTrainee;
    private UserCredentialsDTO userCredentials;

    @BeforeAll
    static void beforeAll() {
        traineeEquals = (trainee1, trainee2) ->
                trainee1.getDateOfBirth().equals(trainee2.getDateOfBirth())
                        && trainee1.getAddress().equals(trainee2.getAddress());

        testTrainee = new Trainee(
                1L,
                "John",
                "Doe",
                "john.doe",
                "password",
                true,
                LocalDate.now(),
                "address");

        trainer1 = new Trainer(
                "Trainer1",
                "LastName",
                "Username",
                "Password",
                true, new TrainingType(TrainingTypeEnum.FITNESS));

        trainer2 = new Trainer(
                "Trainer2",
                "LastName",
                "Username",
                "Password",
                false, new TrainingType(TrainingTypeEnum.ZUMBA));

        training1 = new Training(
                testTrainee,
                trainer1,
                "Training1",
                trainer1.getSpecialization(),
                LocalDate.of(2024, 1, 1),
                20);

        training2 = new Training(
                testTrainee,
                trainer2,
                "Training2",
                trainer2.getSpecialization(),
                LocalDate.of(2023, 12, 11),
                20);

        training3 = new Training(
                testTrainee,
                trainer2,
                "Training3",
                trainer2.getSpecialization(),
                LocalDate.of(2024, 2, 1),
                20);
    }

    @BeforeEach
    void setUp() {
        testTrainee = new Trainee(
                1L,
                "John",
                "Doe",
                "john.doe",
                "password",
                true,
                LocalDate.now(),
                "address");
        
        userCredentials = new UserCredentialsDTO(
                testTrainee.getUsername(),
                testTrainee.getPassword());

        when(traineeRepository.save(any(Trainee.class))).thenAnswer(invocation -> {
            Trainee trainee = invocation.getArgument(0);
            return new Trainee(
                    trainee.getUserId(),
                    trainee.getFirstName(),
                    trainee.getLastName(),
                    trainee.getUsername(),
                    trainee.getPassword(),
                    trainee.isActive(),
                    trainee.getDateOfBirth(),
                    trainee.getAddress());
        });
    
        when(traineeRepository.findByUsernameIgnoreCase(anyString())).thenReturn(Optional.of(testTrainee));
        when(userService.generatePassword()).thenReturn("password-generated");
        doNothing().when(userService).checkCredentials(any(UserCredentialsDTO.class));
    }

    @Test
    void createNewTrainee() {
        when(userService.generateUsername(anyString(), anyString())).thenAnswer(invocation -> {
           String firstName = invocation.getArgument(0);
           String lastName = invocation.getArgument(1);
           return firstName.toLowerCase() + "." + lastName.toLowerCase();
        });

        var traineeCreationDTO = new TraineeCreationDTO(
                testTrainee.getFirstName(),
                testTrainee.getLastName(),
                testTrainee.getDateOfBirth(),
                testTrainee.getAddress());

        Trainee resultTrainee = traineeService.create(traineeCreationDTO);
        assertTrue(traineeEquals.test(testTrainee, resultTrainee));
    }

    @Test
    void updateExistentTrainee() {
        var updatedTrainee = new Trainee(
                "John",
                "Doe",
                "john.doe",
                "password",
                true,
                LocalDate.now(),
                "address");

        var traineeUpdateDTO = new TraineeUpdateDTO(
                testTrainee.getUsername(),
                testTrainee.getFirstName(),
                testTrainee.getLastName(), 
                updatedTrainee.getDateOfBirth(),
                updatedTrainee.getAddress());

        Trainee resultTrainee = traineeService.update(userCredentials, traineeUpdateDTO);
        assertTrue(traineeEquals.test(updatedTrainee, resultTrainee));
    }

    @Test
    void toggleActive() {
        when(userService.toggleActive(any(User.class))).thenAnswer(invocation -> {
            User user = invocation.getArgument(0);
            user.setActive(!user.isActive());
            return user.isActive();
        });

        assertFalse(traineeService.toggleActive(userCredentials, testTrainee.getUsername()));
        assertTrue(traineeService.toggleActive(userCredentials, testTrainee.getUsername()));
    }

    @Test
    void changeTraineePassword() {
        String newPassword = "new-password";

        when(traineeRepository.save(any(Trainee.class))).thenAnswer(invocation -> {
            Trainee trainee = invocation.getArgument(0);
            return new Trainee(
                    trainee.getUserId(),
                    trainee.getFirstName(),
                    trainee.getLastName(),
                    trainee.getUsername(),
                    newPassword,
                    trainee.isActive(),
                    trainee.getDateOfBirth(),
                    trainee.getAddress());
        });

        Trainee trainee = traineeService.changePassword(
                new UserPasswordChangeDTO(
                        new UserCredentialsDTO(
                                testTrainee.getUsername(),
                                testTrainee.getPassword()),
                        newPassword));

        assertEquals(newPassword, trainee.getPassword());
    }

    @Test
    void deleteTrainee() {
        traineeService.delete(
                new UserCredentialsDTO("username", "password"),
                testTrainee.getUsername());

        verify(traineeRepository).delete(testTrainee);
    }

    @Test
    void addTraining() {
        var training = new Training();
        traineeService.addTraining(
                new UserCredentialsDTO("username", "password"),
                training);
        verify(trainingService).create(training);
    }

    @Test
    void getTrainingsFromDate() {
        testTrainee.getTrainings().add(training1);
        testTrainee.getTrainings().add(training2);
        testTrainee.getTrainings().add(training3);

        List<Training> expectedTrainings = Lists.list(training1, training3);
        List<Training> resultTrainings = traineeService.getTrainingsFromDate(
                testTrainee.getUsername(), LocalDate.of(2024, 1, 1));

        assertIterableEquals(expectedTrainings, resultTrainings);
    }

    @Test
    void getTrainingsToDate() {
        testTrainee.getTrainings().add(training1);
        testTrainee.getTrainings().add(training2);
        testTrainee.getTrainings().add(training3);

        List<Training> expectedTrainings = Lists.list(training1, training2);
        List<Training> resultTrainings = traineeService.getTrainingsToDate(
                testTrainee.getUsername(), LocalDate.of(2024, 1, 1));

        assertIterableEquals(expectedTrainings, resultTrainings);
    }

    @Test
    void getTrainingsByTrainerName() {
        testTrainee.getTrainings().add(training1);
        testTrainee.getTrainings().add(training2);
        testTrainee.getTrainings().add(training3);

        List<Training> expectedTrainings = Lists.list(training2, training3);
        List<Training> resultTrainings = traineeService.getTrainingsByTrainerName(
                testTrainee.getUsername(), trainer2.getFirstName());

        assertIterableEquals(expectedTrainings, resultTrainings);
    }

}