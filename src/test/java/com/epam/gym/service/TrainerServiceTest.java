package com.epam.gym.service;

import com.epam.gym.dto.*;
import com.epam.gym.entity.*;
import com.epam.gym.repository.TrainerRepository;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.function.BiPredicate;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@SpringBootTest
class TrainerServiceTest {

    @Autowired
    private TrainerService trainerService;

    @MockBean
    private TrainerRepository trainerRepository;

    @MockBean
    private UserService userService;

    @MockBean
    private TrainingService trainingService;

    private static BiPredicate<Trainer, Trainer> trainerEquals;
    private static Trainee trainee1;
    private static Trainee trainee2;
    private static Training training1;
    private static Training training2;
    private static Training training3;
    private static Trainer testTrainer;
    private UserCredentialsDTO userCredentials;

    @BeforeAll
    static void beforeAll() {
        trainerEquals = (trainer1, trainer2) ->
                trainer1.getSpecialization().getName().equals(trainer2.getSpecialization().getName());

        testTrainer = new Trainer(
                1L,
                "John",
                "Doe",
                "john.doe",
                "password",
                true,
                new TrainingType(TrainingTypeEnum.FITNESS));

        trainee1 = new Trainee(
                "Trainee1",
                "LastName",
                "Username",
                "Password",
                true,
                LocalDate.now(),
                "address");

        trainee2 = new Trainee(
                "Trainee2",
                "LastName",
                "Username",
                "Password",
                true,
                LocalDate.now(),
                "address");

        training1 = new Training(
                trainee1,
                testTrainer,
                "Training1",
                testTrainer.getSpecialization(),
                LocalDate.of(2024, 1, 1),
                20);

        training2 = new Training(
                trainee2,
                testTrainer,
                "Training2",
                testTrainer.getSpecialization(),
                LocalDate.of(2023, 12, 11),
                20);

        training3 = new Training(
                trainee2,
                testTrainer,
                "Training3",
                testTrainer.getSpecialization(),
                LocalDate.of(2024, 2, 1),
                20);
    }

    @BeforeEach
    void setUp() {
        testTrainer = new Trainer(
                1L,
                "John",
                "Doe",
                "john.doe",
                "password",
                true,
                new TrainingType(TrainingTypeEnum.FITNESS));

        userCredentials = new UserCredentialsDTO(
                testTrainer.getUsername(),
                testTrainer.getPassword());

        when(trainerRepository.save(any(Trainer.class))).thenAnswer(invocation -> {
            Trainer trainer = invocation.getArgument(0);
            return new Trainer(
                    trainer.getUserId(),
                    trainer.getFirstName(),
                    trainer.getLastName(),
                    trainer.getUsername(),
                    trainer.getPassword(),
                    trainer.isActive(),
                    trainer.getSpecialization());
        });

        when(trainerRepository.findByUsernameIgnoreCase(anyString())).thenReturn(Optional.of(testTrainer));
        when(userService.generatePassword()).thenReturn("password-generated");
        doNothing().when(userService).checkCredentials(any(UserCredentialsDTO.class));
    }

    @Test
    void createNewTrainer() {
        when(userService.generateUsername(anyString(), anyString())).thenAnswer(invocation -> {
            String firstName = invocation.getArgument(0);
            String lastName = invocation.getArgument(1);
            return firstName.toLowerCase() + "." + lastName.toLowerCase();
        });

        when(userService.generatePassword()).thenReturn("password-generated");

        var trainerCreationDTO = new TrainerCreationDTO(
                testTrainer.getFirstName(),
                testTrainer.getLastName(),
                testTrainer.getSpecialization());

        Trainer resultTrainer = trainerService.create(trainerCreationDTO);
        assertTrue(trainerEquals.test(testTrainer, resultTrainer));
    }

    @Test
    void updateExistentTrainer() {
        var updatedTrainer = new Trainer(
                "John",
                "Doe",
                "john.doe",
                "password",
                true,
                new TrainingType(TrainingTypeEnum.RESISTANCE));

        var trainerUpdateDTO = new TrainerUpdateDTO(
                testTrainer.getUsername(),
                testTrainer.getFirstName(),
                testTrainer.getLastName(),
                updatedTrainer.getSpecialization());

        Trainer resultTrainer = trainerService.update(userCredentials, trainerUpdateDTO);
        assertTrue(trainerEquals.test(updatedTrainer, resultTrainer));
    }

    @Test
    void toggleActive() {
        when(userService.toggleActive(any(User.class))).thenAnswer(invocation -> {
            User user = invocation.getArgument(0);
            user.setActive(!user.isActive());
            return user.isActive();
        });

        assertFalse(trainerService.toggleActive(userCredentials, testTrainer.getUsername()));
        assertTrue(trainerService.toggleActive(userCredentials, testTrainer.getUsername()));
    }

    @Test
    void changeTrainerPassword() {
        String newPassword = "new-password";

        when(trainerRepository.save(any(Trainer.class))).thenAnswer(invocation -> {
            Trainer trainer = invocation.getArgument(0);
            return new Trainer(
                    trainer.getUserId(),
                    trainer.getFirstName(),
                    trainer.getLastName(),
                    trainer.getUsername(),
                    newPassword,
                    trainer.isActive(),
                    trainer.getSpecialization());
        });

        Trainer trainer = trainerService.changePassword(
                new UserPasswordChangeDTO(
                        new UserCredentialsDTO(
                                testTrainer.getUsername(),
                                testTrainer.getPassword()),
                        newPassword));

        assertEquals(newPassword, trainer.getPassword());
    }

    @Test
    void addTraining() {
        var training = new Training();
        trainerService.addTraining(
                new UserCredentialsDTO("username", "password"),
                training);
        verify(trainingService).create(training);
    }

    @Test
    void getTrainingsFromDate() {
        testTrainer.getTrainings().add(training1);
        testTrainer.getTrainings().add(training2);
        testTrainer.getTrainings().add(training3);

        List<Training> expectedTrainings = Lists.list(training1, training3);
        List<Training> resultTrainings = trainerService.getTrainingsFromDate(
                testTrainer.getUsername(), LocalDate.of(2024, 1, 1));

        assertIterableEquals(expectedTrainings, resultTrainings);
    }

    @Test
    void getTrainingsToDate() {
        testTrainer.getTrainings().add(training1);
        testTrainer.getTrainings().add(training2);
        testTrainer.getTrainings().add(training3);

        List<Training> expectedTrainings = Lists.list(training1, training2);
        List<Training> resultTrainings = trainerService.getTrainingsToDate(
                testTrainer.getUsername(), LocalDate.of(2024, 1, 1));

        assertIterableEquals(expectedTrainings, resultTrainings);
    }

    @Test
    void getTrainingsByTrainerName() {
        testTrainer.getTrainings().add(training1);
        testTrainer.getTrainings().add(training2);
        testTrainer.getTrainings().add(training3);

        List<Training> expectedTrainings = Lists.list(training2, training3);
        List<Training> resultTrainings = trainerService.getTrainingsByTraineeName(
                testTrainer.getUsername(), trainee2.getFirstName());

        assertIterableEquals(expectedTrainings, resultTrainings);
    }

}