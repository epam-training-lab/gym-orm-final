package com.epam.gym.dto;

public interface UserCreationDTO {
    
    String firstName();
    String lastName();

}
