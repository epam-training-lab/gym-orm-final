package com.epam.gym.dto;

import jakarta.validation.constraints.NotBlank;

import java.time.LocalDate;

public record TraineeCreationDTO(
        @NotBlank
        String firstName,
        @NotBlank
        String lastName,
        LocalDate dateOfBirth,
        String address
) implements UserCreationDTO {

}
