package com.epam.gym.dto;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

public record UserPasswordChangeDTO(
        @NotNull
        UserCredentialsDTO userCredentials,
        @NotBlank
        String newPassword
) {

}
