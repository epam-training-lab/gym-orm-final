package com.epam.gym.dto;

import jakarta.validation.constraints.NotBlank;

import java.time.LocalDate;

public record TraineeUpdateDTO(
        @NotBlank
        String username,
        @NotBlank
        String firstName,
        @NotBlank
        String lastName,
        LocalDate dateOfBirth,
        String address
) {

}
