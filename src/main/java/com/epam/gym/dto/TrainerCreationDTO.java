package com.epam.gym.dto;

import com.epam.gym.entity.TrainingType;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

public record TrainerCreationDTO(
        @NotBlank
        String firstName,
        @NotBlank
        String lastName,
        @NotNull
        TrainingType specialization
) implements UserCreationDTO {

}
