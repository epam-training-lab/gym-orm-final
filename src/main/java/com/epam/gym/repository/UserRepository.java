package com.epam.gym.repository;

import com.epam.gym.entity.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {

    boolean existsByUsernameAndPassword(String username, String password);
    int countByUsernameStartingWith(String username);

}
