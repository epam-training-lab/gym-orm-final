package com.epam.gym.repository;

import com.epam.gym.entity.TrainingType;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TrainingTypeRepository extends CrudRepository<TrainingType, Long> {

}
