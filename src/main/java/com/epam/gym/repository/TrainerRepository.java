package com.epam.gym.repository;

import com.epam.gym.entity.Trainer;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TrainerRepository extends CrudRepository<Trainer, Long> {

    Optional<Trainer> findByUsernameIgnoreCase(String username);

}
