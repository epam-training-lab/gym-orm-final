package com.epam.gym.exception;

public class TrainerNotFoundException extends RuntimeException {
    
    public TrainerNotFoundException() {
        super("Trainer not found");
    }

    public TrainerNotFoundException(String message) {
        super(message);
    }
    
}
