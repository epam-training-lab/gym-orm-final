package com.epam.gym.exception;

public class TrainingNotFoundException extends RuntimeException {
    
    public TrainingNotFoundException() {
        super("Training not found");
    }

    public TrainingNotFoundException(String message) {
        super(message);
    }
    
}
