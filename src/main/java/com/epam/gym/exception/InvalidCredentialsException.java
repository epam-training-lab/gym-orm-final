package com.epam.gym.exception;

public class InvalidCredentialsException extends RuntimeException {

    public InvalidCredentialsException() {
        super("Invalid user credentials");
    }

    public InvalidCredentialsException(String message) {
        super(message);
    }
}
