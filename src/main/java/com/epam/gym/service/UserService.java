package com.epam.gym.service;

import com.epam.gym.dto.UserCredentialsDTO;
import com.epam.gym.exception.InvalidCredentialsException;
import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.gym.entity.User;
import com.epam.gym.repository.UserRepository;

import java.util.Random;

@Service
@Slf4j
public class UserService {

    private final UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public boolean toggleActive(User user) {
        user.setActive(!user.isActive());
        return user.isActive();
    }

    String generateUsername(String firstName, String lastName) {
        String username = firstName.toLowerCase() + "." + lastName.toLowerCase();
        int usernameCount = userRepository.countByUsernameStartingWith(username);

        if (usernameCount > 0) {
            username += usernameCount;
            log.debug("Username already exists. Serial number attached: " + usernameCount);
        }

        return username;
    }

    String generatePassword() {
        final int PASSWORD_LENGTH = 10;
        final int ASCII_CODE_START = 33;
        final int ASCII_CODE_END = 126;

        var password = new StringBuilder(PASSWORD_LENGTH);
        var random = new Random();

        while (password.length() < PASSWORD_LENGTH) {
            int randomASCII = random.nextInt(ASCII_CODE_START, ASCII_CODE_END + 1);
            password.append((char) randomASCII);
        }

        return password.toString();
    }

    public boolean invalidCredentials(UserCredentialsDTO userCredentials) {
        return !userRepository.existsByUsernameAndPassword(
                userCredentials.username(),
                userCredentials.password());
    }

    public void checkCredentials(UserCredentialsDTO userCredentials) {
        if (invalidCredentials(userCredentials)) {
            log.warn("Invalid user credentials");
            throw new InvalidCredentialsException();
        }
    }

}
