package com.epam.gym.service;

import com.epam.gym.dto.TraineeCreationDTO;
import com.epam.gym.dto.TraineeUpdateDTO;
import com.epam.gym.dto.UserCredentialsDTO;
import com.epam.gym.dto.UserPasswordChangeDTO;
import com.epam.gym.entity.Trainee;
import com.epam.gym.entity.Training;
import com.epam.gym.exception.TraineeNotFoundException;
import com.epam.gym.repository.TraineeRepository;

import lombok.Synchronized;
import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;

@Service
@Slf4j
public class TraineeService {

    private final TraineeRepository traineeRepository;
    private final UserService userService;
    private final TrainingService trainingService;

    @Autowired
    public TraineeService(TraineeRepository traineeRepository, UserService userService,
            TrainingService trainingService) {
        this.traineeRepository = traineeRepository;
        this.userService = userService;
        this.trainingService = trainingService;
    }

    @Synchronized
    public Trainee create(TraineeCreationDTO traineeDTO) {
        var trainee = new Trainee(
                traineeDTO.firstName(),
                traineeDTO.lastName(),
                userService.generateUsername(
                    traineeDTO.firstName(),
                    traineeDTO.lastName()),
                userService.generatePassword(),
                true,
                traineeDTO.dateOfBirth(),
                traineeDTO.address());

        trainee = traineeRepository.save(trainee);

        log.info("New trainee created: UserID=" + trainee.getUserId());
        return trainee;
    }

    public Trainee update(UserCredentialsDTO userCredentials, TraineeUpdateDTO traineeUpdateDTO) {
        userService.checkCredentials(userCredentials);
        Trainee trainee = select(traineeUpdateDTO.username());

        trainee = new Trainee(
                trainee.getUserId(),
                traineeUpdateDTO.firstName(),
                traineeUpdateDTO.lastName(),
                traineeUpdateDTO.username(),
                trainee.getPassword(),
                trainee.isActive(),
                traineeUpdateDTO.dateOfBirth(),
                traineeUpdateDTO.address());

        trainee = traineeRepository.save(trainee);

        log.info("Trainee updated: UserID=" + trainee.getUserId());
        return trainee;
    }

    public boolean toggleActive(UserCredentialsDTO userCredentials, String username) {
        userService.checkCredentials(userCredentials);
        Trainee trainee = select(username);
        boolean isActive = userService.toggleActive(trainee);
        log.info("Trainee 'active' set to " + isActive + ": UserID=" + trainee.getUserId());
        return trainee.isActive();
    }

    public Trainee changePassword(UserPasswordChangeDTO passwordChangeDTO) {
        userService.checkCredentials(passwordChangeDTO.userCredentials());
        Trainee trainee = select(passwordChangeDTO.userCredentials().username());

        trainee = new Trainee(
            trainee.getUserId(),
            trainee.getFirstName(),
            trainee.getLastName(),
            trainee.getUsername(),
            userService.generatePassword(),
            trainee.isActive(),
            trainee.getDateOfBirth(),
            trainee.getAddress());

        log.info("Trainee password changed: UserID=" + trainee.getUserId());
        return traineeRepository.save(trainee);
    }

    public Trainee select(String username) {
        return traineeRepository.findByUsernameIgnoreCase(username)
                .orElseThrow(TraineeNotFoundException::new);
    }

    @Transactional
    public void delete(UserCredentialsDTO userCredentials, String username) {
        userService.checkCredentials(userCredentials);
        Trainee trainee = select(username);
        trainingService.deleteAll(trainee.getTrainings());
        traineeRepository.delete(trainee);
        log.info("Trainee deleted: UserID=" + trainee.getUserId());
    }

    public void addTraining(UserCredentialsDTO userCredentials, Training training) {
        userService.checkCredentials(userCredentials);
        trainingService.create(training);
        log.info("Training added: UserID=" + training.getId());
    }

    public List<Training> getTrainingsFromDate(String username, LocalDate fromDate) {
        Trainee trainee = select(username);
        return trainee.getTrainings().stream()
                .filter(training -> !training.getTrainingDate().isBefore(fromDate))
                .toList();
    }

    public List<Training> getTrainingsToDate(String username, LocalDate toDate) {
        Trainee trainee = select(username);
        return trainee.getTrainings().stream()
                .filter(training -> !training.getTrainingDate().isAfter(toDate))
                .toList();
    }

    public List<Training> getTrainingsByTrainerName(String username, String trainerName) {
        Trainee trainee = select(username);
        return trainee.getTrainings().stream()
                .filter(training -> training
                        .getTrainer()
                        .getFirstName()
                        .equalsIgnoreCase(trainerName))
                .toList();
    }

}
