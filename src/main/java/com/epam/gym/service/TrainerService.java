package com.epam.gym.service;

import com.epam.gym.dto.TrainerUpdateDTO;
import com.epam.gym.dto.UserCredentialsDTO;
import com.epam.gym.dto.UserPasswordChangeDTO;
import com.epam.gym.entity.Training;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.gym.dto.TrainerCreationDTO;
import com.epam.gym.entity.Trainer;
import com.epam.gym.exception.TrainerNotFoundException;
import com.epam.gym.repository.TrainerRepository;

import lombok.Synchronized;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.time.LocalDate;

@Service
@Slf4j
public class TrainerService {

    private final TrainerRepository trainerRepository;
    private final UserService userService;
    private final TrainingService trainingService;

    @Autowired
    public TrainerService(TrainerRepository trainerDAO, UserService userService,
        TrainingService trainingService) {
        this.trainerRepository = trainerDAO;
        this.userService = userService;
        this.trainingService = trainingService;
    }

    @Synchronized
    public Trainer create(TrainerCreationDTO trainerDTO) {
        var trainer = new Trainer(
            trainerDTO.firstName(),
            trainerDTO.lastName(),
            userService.generateUsername(
                trainerDTO.firstName(),
                trainerDTO.lastName()),
                userService.generatePassword(),
                true,
                trainerDTO.specialization());

        trainer = trainerRepository.save(trainer);

        log.info("New trainer created: UserID=" + trainer.getUserId());
        return trainer;
    }

    public Trainer update(UserCredentialsDTO userCredentials, TrainerUpdateDTO trainerUpdateDTO) {
        userService.checkCredentials(userCredentials);
        Trainer trainer = select(trainerUpdateDTO.username());

        trainer = new Trainer(
                trainer.getUserId(),
                trainerUpdateDTO.firstName(),
                trainerUpdateDTO.lastName(),
                trainerUpdateDTO.username(),
                trainer.getPassword(),
                trainer.isActive(),
                trainerUpdateDTO.specialization());

        trainer = trainerRepository.save(trainer);

        log.info("Trainer updated: UserID=" + trainer.getUserId());
        return trainer;
    }

    public boolean toggleActive(UserCredentialsDTO userCredentials, String username) {
        userService.checkCredentials(userCredentials);
        Trainer trainer = select(username);
        boolean isActive = userService.toggleActive(trainer);
        log.info("Trainer 'active' set to " + isActive + ": UserID=" + trainer.getUserId());
        return trainer.isActive();
    }

    public Trainer changePassword(UserPasswordChangeDTO passwordChangeDTO) {
        userService.checkCredentials(passwordChangeDTO.userCredentials());
        Trainer trainer = select(passwordChangeDTO.userCredentials().username());

        trainer = new Trainer(
        trainer.getUserId(),
        trainer.getFirstName(),
        trainer.getLastName(),
        trainer.getUsername(),
        userService.generatePassword(),
        trainer.isActive(),
        trainer.getSpecialization());

        log.info("Trainer password changed: UserID=" + trainer.getUserId());
        return trainerRepository.save(trainer);
    }

     public Trainer select(String username) {
         return trainerRepository.findByUsernameIgnoreCase(username)
                 .orElseThrow(TrainerNotFoundException::new);
     }

     public void addTraining(UserCredentialsDTO userCredentials, Training training) {
         userService.checkCredentials(userCredentials);
         trainingService.create(training);
         log.info("Training added: UserID=" + training.getId());
     }

    public List<Training> getTrainingsFromDate(String username, LocalDate fromDate) {
        Trainer trainer = select(username);
        return trainer.getTrainings().stream()
                .filter(training -> !training.getTrainingDate().isBefore(fromDate))
                .toList();
    }

    public List<Training> getTrainingsToDate(String username, LocalDate toDate) {
        Trainer trainer = select(username);
        return trainer.getTrainings().stream()
                .filter(training -> !training.getTrainingDate().isAfter(toDate))
                .toList();
    }

    public List<Training> getTrainingsByTraineeName(String username, String traineeName) {
        Trainer trainer = select(username);
        return trainer.getTrainings().stream()
                .filter(training -> training
                        .getTrainee()
                        .getFirstName()
                        .equalsIgnoreCase(traineeName))
                .toList();
    }

}
