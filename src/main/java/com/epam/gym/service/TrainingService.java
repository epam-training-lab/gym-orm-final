package com.epam.gym.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.gym.entity.Training;
import com.epam.gym.repository.TrainingRepository;

import java.util.List;

@Service
public class TrainingService {

    private final TrainingRepository trainingRepository;

    @Autowired
    public TrainingService(TrainingRepository trainingRepository) {
        this.trainingRepository = trainingRepository;
    }

    public Training create(Training training) {
        return trainingRepository.save(training);
    }

    public void deleteAll(List<Training> trainings) {
        trainingRepository.deleteAll(trainings);
    }

}
