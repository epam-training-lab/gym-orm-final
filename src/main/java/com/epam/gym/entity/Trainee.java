package com.epam.gym.entity;

import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "trainee")
@Getter
@NoArgsConstructor
public class Trainee extends User {

    @Column(name = "date_of_birth")
    private LocalDate dateOfBirth;

    @Column(name = "address")
    private String address;

    @ManyToMany(mappedBy = "trainees")
    private List<Trainer> trainers;

    @OneToMany(mappedBy = "trainee")
    private List<Training> trainings;

    public Trainee(String firstName, String lastName, String username, String password,
            boolean isActive, LocalDate dateOfBirth, String address) {
        super(firstName, lastName, username, password, isActive);
        this.dateOfBirth = dateOfBirth;
        this.address = address;
    }

    public Trainee(Long userId, String firstName, String lastName, String username,
            String password, boolean isActive, LocalDate dateOfBirth, String address) {
        super(userId, firstName, lastName, username, password, isActive);
        this.dateOfBirth = dateOfBirth;
        this.address = address;
        this.trainings = new ArrayList<>();
    }

}
