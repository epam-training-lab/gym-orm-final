package com.epam.gym.entity;

public enum TrainingTypeEnum {
    FITNESS ("Fitness"),
    YOGA ("Yoga"),
    ZUMBA ("Zumba"),
    STRETCHING ("Stretching"),
    RESISTANCE ("Resistance");

    private final String name;

    TrainingTypeEnum(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return this.name;
    }
    
}
