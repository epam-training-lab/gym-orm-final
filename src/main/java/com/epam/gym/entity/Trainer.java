package com.epam.gym.entity;

import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.*;
import lombok.*;

@Entity
@Table(name = "trainer")
@Getter
@NoArgsConstructor
public class Trainer extends User {

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "specialization", nullable = false)
    private TrainingType specialization;

    @ManyToMany
    @JoinTable(
            name = "trainee2trainer",
            joinColumns = @JoinColumn(name = "trainer_id"),
            inverseJoinColumns = @JoinColumn(name = "trainee_id")
    )
    private List<Trainee> trainees;

    @OneToMany(mappedBy = "trainer")
    private List<Training> trainings;

    public Trainer(String firstName, String lastName, String username, String password,
            boolean isActive, TrainingType specialization) {
        super(firstName, lastName, username, password, isActive);
        this.specialization = specialization;
    }

    public Trainer(Long userId, String firstName, String lastName, String username,
            String password, boolean isActive, TrainingType specialization) {
        super(userId, firstName, lastName, username, password, isActive);
        this.specialization = specialization;
        trainings = new ArrayList<>();
    }

}
