package com.epam.gym.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import lombok.Getter;
import lombok.ToString;

@Entity
@Table(name = "training_type")
@Getter
@ToString
@NoArgsConstructor
public class TrainingType {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "name", nullable = false)
    private TrainingTypeEnum name;

    public TrainingType(TrainingTypeEnum name) {
        this.name = name;
    }

}
